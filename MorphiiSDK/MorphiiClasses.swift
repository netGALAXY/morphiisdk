//
//  Classes.swift
//  MorphiiSDK
//
//  Created by Charles von Lehe on 1/11/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation

@objc public class MorphiiSDK: NSObject {
    
    public class Size {
        public var initialSize:Int! //For future use
        public var selectedSize:Int! // Currently valid only for single morphii config.
        
        public init (initialSize:Int, selectedSize:Int) {
            self.initialSize = initialSize
            self.selectedSize = selectedSize
        }
    }
    
    @objc public class Coordinates: NSObject {
        public var lat:Double!
        public var lng:Double!
        
        public init (lat:Double, lng:Double) {
            self.lat = lat
            self.lng = lng
        }
    }
    
    @objc public class Morphii: NSObject {
        public var id:String!
        public var name:String!
        public var weight:Int!
        public var size:Size!
        
        public init (id:String, name:String, weight:Int, size:Size) {
            self.id = id
            self.name = name
            self.weight = weight
            self.size = size
        }
    }
    
    @objc public class MorphiiList: NSObject {
        public var ids:[Morphii] = []
        public var showName:Bool!
        
        public init(showName:Bool) {
            self.showName = showName
        }
        
        public func add (id:String, name:String, weight:Int, size:MorphiiSDK.Size) {
            ids.append(Morphii(id: id, name: name, weight: weight, size: size))
        }
        
        public func getIDs () -> [String] {
            var idStrings:[String] = []
            for morphii in ids {
                idStrings.append(morphii.id)
            }
            return idStrings
        }
    }
    
    public class Selection {
        public var required = true
        
        public init (required:Bool?) {
            if let req = required {
                self.required = req
            }
        }
    }
    
    public class Options {
        public var stage = Stage.LIVE
        public var initialIntensity = 0.5 {
            didSet {
                if initialIntensity > 1 {
                    initialIntensity = 1.0
                }else if initialIntensity < 0 {
                    initialIntensity = 0.0
                }
            }
        }
        
        public enum Stage:String {
            case TEST = "test"
            case VALIDATION = "validation"
            case LIVE = "live"
        }
        
        public init (stage:Stage?, initialIntensity:Double?) {
            if let s = stage {
                self.stage = s
            }
            if let initial = initialIntensity {
                self.initialIntensity = initial
            }
        }
    }
   

    
    @objc public class ReactionResult: NSObject {
        public var isSubmitted:Bool!
        public var viewId:String!
        public var reactionId:String!
        public var reactionRecord:[String:String]!
        
        public init (isSubmitted:Bool, viewId:String, reactionId:String, reactionRecord:[String:String]) {
            self.isSubmitted = isSubmitted
            self.viewId = viewId
            self.reactionId = reactionId
            self.reactionRecord = reactionRecord
        }
    }
    
    @objc public class AuthenticationResults: NSObject {
        public var isAuthenticated = false
        private var jwt:String?
        private var apiKey:String?
        private var sessionId:String?
        public var error:ReactionError?
        
        public init (isAuthenticated:Bool, jwt:String?, apiKey:String?, sessionId:String?, error:ReactionError?) {
            self.isAuthenticated = isAuthenticated
            self.jwt = jwt
            self.apiKey = apiKey
            self.sessionId = sessionId
            self.error = error
        }
    }
    
}
