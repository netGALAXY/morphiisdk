//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the comment reaction data
public class ReactionCommentRecord {
    
    public let text: String
    public let locale: String
    
    public init(text: String, locale: String) {
        self.text = text
        self.locale = locale
    }
}
