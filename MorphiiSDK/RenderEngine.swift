//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import UIKit
import Foundation


// This class contains the logic to render the morphii metadata.
class RenderEngine {
   
   var intensity: Double
   let anchorDict: NSDictionary
   let deltasDict: NSDictionary
   let deltaDict: NSDictionary
   
   init(morphiiData: MorphiiData, intensity: Double) {
      self.intensity = intensity
      
      anchorDict = morphiiData.metadata.value(forKey: "anchor") as! NSDictionary
      deltasDict = morphiiData.metadata.value(forKey: "deltas") as! NSDictionary
      deltaDict = deltasDict.value(forKey: "DELTA1") as! NSDictionary
   }
   
   private struct Point{
      var x: NSNumber! = 0
      var y: NSNumber! = 0
   }
   
   private struct Curve {
      var index: Int! = 0
      var x1: NSNumber! = 0.0
      var y1: NSNumber! = 0.0
      var x2: NSNumber! = 0.0
      var y2: NSNumber! = 0.0
      var x:  NSNumber! = 0.0
      var y:  NSNumber! = 0.0
   }
   
   private struct Segment {
      var key: String
      var color: AnyObject
      var st = Point()
      var curvs: [Curve]?
   }
   
   // This function renders the morphii metadata to the given context.
   func render(context:CGContext) -> CGContext {
      
      var intensityVal = (self.intensity / 100)
      intensityVal = intensityVal > 1 ? 1 : intensityVal
      intensityVal = intensityVal < 0 ? 0 : intensityVal
      
      let morphii: [Segment] = setPathSegs(self.anchorDict, intensity: (intensityVal * -1), delta: self.deltaDict)
      
      let sortedMorphii = morphii.sorted(by: { $0.key < $1.key })
      for seg in sortedMorphii {
         
         let morphiiSeg = seg
         let color = morphiiSeg.color
         let st = morphiiSeg.st
         let curvs = morphiiSeg.curvs!
         
         color.setFill()
         context.beginPath()
         context.move(to: CGPoint(x: CGFloat(st.x), y: CGFloat(st.y)))
         
         for(i,_) in curvs.enumerated(){
            
            let curv = curvs[i]
            
            let curvx1 = curv.x1
            let curvy1 = curv.y1
            let curvx2 = curv.x2
            let curvy2 = curv.y2
            let curvx = curv.x
            let curvy = curv.y
            
            let mtp = CGPoint(x: CGFloat(curvx!), y: CGFloat(curvy!))
            let pt1 = CGPoint(x: CGFloat(curvx1!), y: CGFloat(curvy1!))
            let pt2 = CGPoint(x: CGFloat(curvx2!), y: CGFloat(curvy2!))
            
            context.addCurve(to: mtp, control1: pt1, control2: pt2)
         }
         context.closePath()
         context.fillPath()
      }
      return context
   }
   
   private func calculatePath(_ seg: NSString, anchorSeg: NSDictionary, intensity: Double, deltaSeg: NSDictionary) -> Segment {
      
      // Convert hex color to UIColor
      let segHexColor:String = anchorSeg.value(forKeyPath: "color") as! String
      let segColor:UIColor = colorWithHexString(segHexColor)
      
      let stx = anchorSeg.value(forKeyPath: "st.x") as! Double + (intensity * (deltaSeg.value(forKeyPath: "st.x") as! Double))
      let sty = anchorSeg.value(forKeyPath: "st.y") as! Double + (intensity * (deltaSeg.value(forKeyPath: "st.y") as! Double))
      
      var segment = Segment(key: seg as String, color:segColor, st: Point(x: stx as NSNumber!, y: sty as NSNumber!), curvs: nil)
      var curveSegment = [ Curve ]()
      
      let segNumb:Int = anchorSeg.count
      for s in (1 ..< (segNumb - 2)) {
         let curvx1 = "c\(s).x1"
         let curvy1 = "c\(s).y1"
         let curvx2 = "c\(s).x2"
         let curvy2 = "c\(s).y2"
         let curvx  = "c\(s).x"
         let curvy  = "c\(s).y"
         
         let cix1 = anchorSeg.value(forKeyPath: curvx1) as! Double + (intensity * (deltaSeg.value(forKeyPath: curvx1) as! Double))
         let ciy1 = anchorSeg.value(forKeyPath: curvy1) as! Double + (intensity * (deltaSeg.value(forKeyPath: curvy1) as! Double))
         let cix2 = anchorSeg.value(forKeyPath: curvx2) as! Double + (intensity * (deltaSeg.value(forKeyPath: curvx2) as! Double))
         let ciy2 = anchorSeg.value(forKeyPath: curvy2) as! Double + (intensity * (deltaSeg.value(forKeyPath: curvy2) as! Double))
         let cix  = anchorSeg.value(forKeyPath: curvx) as! Double + (intensity * (deltaSeg.value(forKeyPath: curvx) as! Double))
         let ciy  = anchorSeg.value(forKeyPath: curvy) as! Double + (intensity * (deltaSeg.value(forKeyPath: curvy) as! Double))
         
         let curve = Curve(index: s, x1: cix1 as NSNumber!, y1: ciy1 as NSNumber!, x2: cix2 as NSNumber!, y2: ciy2 as NSNumber!, x: cix as NSNumber!, y: ciy as NSNumber!)
         
         curveSegment.append(curve)
         segment.curvs = curveSegment
      }
      return segment
   }
   
   private func setPathSegs(_ anchor: NSDictionary?, intensity: Double, delta: NSDictionary) -> [ Segment ] {
      
      var paths = [ Segment ]()
      if let anchor = anchor {
         for(key, _) in anchor {
            let anchorPart:NSDictionary = anchor.value(forKeyPath: key as! String)as! NSDictionary
            let deltaPart:NSDictionary = delta.value(forKeyPath: key as! String)as! NSDictionary
            let path = calculatePath(key as! String as NSString, anchorSeg: anchorPart, intensity: intensity, deltaSeg: deltaPart)
            paths.append(path)
         }
      }
      
      return paths
   }
   
   private func colorWithHexString (_ hex: String) -> UIColor {
      
      var hexString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
      if (hexString.hasPrefix("#")) {
         hexString = (hexString as NSString).substring(from: 1)
      }
      
      if (hexString.characters.count != 6) {
         return UIColor.gray
      }
      
      let rString = (hexString as NSString).substring(to: 2)
      let gString = ((hexString as NSString).substring(from: 2) as NSString).substring(to: 2)
      let bString = ((hexString as NSString).substring(from: 4) as NSString).substring(to: 2)
      
      var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
      Scanner(string: rString).scanHexInt32(&r)
      Scanner(string: gString).scanHexInt32(&g)
      Scanner(string: bString).scanHexInt32(&b)
      
      return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
   }
}


// Helper for hex function
extension String
{
   func trim() -> String
   {
      return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
   }
}

