//
//  MorphiiManager.swift
//  MorphiiSDK
//
//  Created by Charles von Lehe on 1/6/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation


public class MorphiiManager {
    
    public class func fetchAllMorphiis(_ completion: @escaping (_ morphiisArray: [ Morphii ]) -> Void ) -> Void {
        //confirmInitialization()
        let queue = DispatchQueue(label: "com.app.queue")
        queue.async{
            
            let morphiiApi = MorphiiAPI()
            //let newMorphiis = morphiiApi.fetchAllMorphiis()
            morphiiApi.fetchAllMorphiis() { (morphiiArray) in
                print("MORPHIIS ARRAY HAS THESE INSIDE \(morphiiArray)")
                completion(morphiiArray)
            }
            
            
        }
    }
    
    public class func initialize (username:String, password:String, accountId:String, clientKey:String, completion:@escaping (_ success:Bool, _ errorCode:Int?) -> Void) {
        MorphiiAPI.username = username
        MorphiiAPI.password = password
        MorphiiAPI.accountId = accountId
        MorphiiAPI.clientKey = clientKey
        
//        MorphiiAPI().authenticate(username: username, password: password, accountId: accountId, clientKey: clientKey, completion: completion)
        
        MorphiiService.sharedInstance().authenticate(username: username, password: password, accountId: accountId) { (authenticationResults) in
            print("AUTHENTICATION_RESULTS:",authenticationResults)
        }
        
        
    }
    
    class func confirmInitialization () {
        if MorphiiAPI.username == nil || MorphiiAPI.password == nil || MorphiiAPI.accountId == nil || MorphiiAPI.clientKey == nil {
            fatalError("The Morphii SDK has not been initialized. You have to call MorphiiManager.initialize()")
        }
    }
}
