//
//  EventHandler.swift
//  MorphiiSDK
//
//  Created by Charles von Lehe on 1/11/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation

class MorphiiEventHandler  {
    
    class func callSelectionChange (selectionRequiredValid:Bool, viewObject:ViewObject) {
        let morphii = ["selected":viewObject.morphii.selected, "weight":viewObject.morphii.weight] as [String:Any]
        let view = ["id":viewObject.id, "morphii":morphii] as [String:Any]
        let _ = ["type":"selection_changed", "selectionRequiredValid":selectionRequiredValid, "view":view] as [String : Any]
    }
    
    class ViewObject {
        var id = ""
        var morphii:MorphiiObject!
        
        init(id:String, morphiiObject:MorphiiObject) {
            self.id = id
            self.morphii = morphiiObject
        }
        
        class MorphiiObject {
            var selected = false
            var weight = 0
            
            init(selected:Bool, weight:Int) {
                self.selected = selected
                self.weight = weight
            }
        }
    }
}
