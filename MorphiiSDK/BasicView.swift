//
//  BasicView.swift
//  MorphiiSDK
//
//  Created by Charles von Lehe on 1/10/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit

public protocol BasicViewDelegate {
    func selectionDidChange (type:String, selectionRequiredValid:Bool, view:BasicView.BasicViewObject)
   func commentChange (type:String, commentRequiredValid:Bool, view:BasicView.BasicViewObject)
}

@objc public class BasicView: UIView {
    private var id:String!
    private(set) var morphiiSelected = false
    private(set) var reaction:MorphiiSDK.ReactionResult?
    var targetMetadata:[String:String] = [:]
   var currentMorphiiView:MorphiiView?
    public var delegate:BasicViewDelegate?
   var configurtation:BasicViewConfiguration!
   var viewId:String!
   var commentTextView:UITextView?
    var morphiiLayout:UIView?
   var isSubmitted = false
   var morphiiViews:[MorphiiView] = []
   var placeholder = ""
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
   
   public init (frame:CGRect, configuration:BasicViewConfiguration, delegate:BasicViewDelegate?) {
      super.init(frame: frame)
      self.delegate = delegate
    self.configurtation = configuration
      MorphiiAPI().fetchMorphiis(ids: configuration.morphiiConfig.ids()) { (morphiis) in
         print("MORPHIIS1:",morphiis)
         self.placeholder = configuration.comment.hintText
        if configuration.morphiiConfig.ids().count == 1 {
            print("MORPHIIS2:",morphiis)
         
            if configuration.comment.show {
                print("MORPHIIS3:",morphiis)
                if let morphii = morphiis.first {
                    print("MORPHIIS4:",morphiis)
                    self.setupSingleMorphiiCommentView(morphii: morphii, configuration: configuration)
                }
            }else {
                print("MORPHIIS5:",morphiis)

                if let morphii = morphiis.first {
                    print("MORPHIIS6:",morphiis)
                    self.setupSingleMorphiiView(morphii: morphii, configuration: configuration)
                }
            }
        }else if configuration.morphiiConfig.ids().count > 1 {
            print("MORPHIIS7:",morphiis)
            if configuration.comment.show {
                print("MORPHIIS8:",morphiis)
               self.setupMultiMorphiiCommentView(morphiis: morphiis, configuration: configuration)
            }else {
                print("MORPHIIS9:",morphiis)
               self.setupMultiMorphiiView(morphiis: morphiis, configuration: configuration)

            }
        }

    }
    

   }
   
   func setupMultiMorphiiView (morphiis:[Morphii], configuration:BasicViewConfiguration) {
      let bundle = Bundle(for: self.classForCoder)
      guard let multiMorphiiView = bundle.loadNibNamed("MultiMorphiiNoCommentView", owner: self, options: nil)?.first as? MultiMorphiiNoCommentView else {
         return
      }
    morphiiLayout = multiMorphiiView
    
      let viewId = UUID().uuidString
      self.viewId = viewId
      multiMorphiiView.viewId = viewId
      self.addSubview(multiMorphiiView)
      multiMorphiiView.bindFrameToSuperviewBounds()
      
    multiMorphiiView.setupMorphiis(morphiiArray: morphiis, configuration: configuration, basicView:self)
    
   }
   
   func setupMultiMorphiiCommentView (morphiis:[Morphii], configuration:BasicViewConfiguration) {
      let bundle = Bundle(for: self.classForCoder)
      guard let multiMorphiiView = bundle.loadNibNamed("MultiMorphiiCommentView", owner: self, options: nil)?.first as? MultiMorphiiCommentView else {
         return
      }
    commentTextView = multiMorphiiView.commentTextView
    morphiiLayout = multiMorphiiView
    multiMorphiiView.placeHolder = configuration.comment.hintText
    multiMorphiiView.commentTitleLabel.text = configuration.comment.label
      let viewId = UUID().uuidString
      self.viewId = viewId
      multiMorphiiView.viewId = viewId
      self.addSubview(multiMorphiiView)
      multiMorphiiView.bindFrameToSuperviewBounds()
    multiMorphiiView.setupMorphiis(morphiiArray: morphiis, configuration: configuration, basicView:self)

   }
   
    func setupSingleMorphiiView (morphii:Morphii, configuration:BasicViewConfiguration) {
        let bundle = Bundle(for: self.classForCoder)
        guard let singleMorphiiView = bundle.loadNibNamed("SingleMorphiiView", owner: self, options: nil)?.first as? SingleMorphiiView else {
            return
        }
        morphiiLayout = singleMorphiiView
        let viewId = UUID().uuidString
        self.viewId = viewId
        singleMorphiiView.viewId = viewId
        self.addSubview(singleMorphiiView)
        singleMorphiiView.bindFrameToSuperviewBounds()
        
        var metaData = NSDictionary()
        if let data = morphii.metaData {
            metaData = data
        }
        var displayName = ""
        if let display = configuration.morphiiConfig.displayNameById(id: morphii.id) {
            displayName = display
        }
        
        let currentMorphii = MorphiiData(id: morphii.id, name: morphii.name, scaleType: morphii.scaleType, metadata: metaData, displayName: displayName)
        self.currentMorphiiView = singleMorphiiView.morphiiView
        self.morphiiViews.append(singleMorphiiView.morphiiView)
        if configuration.morphiiConfig.showName {
            singleMorphiiView.nameLabel.text = morphii.name
        }else {
            singleMorphiiView.nameLabel.text = ""
        }
      let sideLength = frame.size.height - 20.0
      print("SIDE_LENGTH:",sideLength)
      let size = CGSize(width: sideLength, height: sideLength)

        singleMorphiiView.morphiiView.setUpMorphii(target: configuration.target, viewId: viewId, currentMorphii, size: size, intensity: configuration.options.initialIntensity)
        sendSelectionChangeEvent()
      singleMorphiiView.morphiiView.intensity = configuration.options.initialIntensity


    }
    
    func setupSingleMorphiiCommentView (morphii:Morphii, configuration:BasicViewConfiguration) {
        let bundle = Bundle(for: self.classForCoder)
        guard let singleMorphiiView = bundle.loadNibNamed("SingleMorphiiCommentView", owner: self, options: nil)?.first as? SingleMorphiiCommentView else {
            return
        }
        singleMorphiiView.basicView = self
        commentTextView = singleMorphiiView.commentTextView
        morphiiLayout = singleMorphiiView
        singleMorphiiView.placeHolder = configuration.comment.hintText
        singleMorphiiView.commentTitleLabel.text = configuration.comment.label
        let viewId = UUID().uuidString
        self.viewId = viewId
        self.addSubview(singleMorphiiView)
        singleMorphiiView.bindFrameToSuperviewBounds()
        
        var metaData = NSDictionary()
        if let data = morphii.metaData {
            metaData = data
        }
        var displayName = ""
        if let display = configuration.morphiiConfig.displayNameById(id: morphii.id) {
            displayName = display
        }
        
        let currentMorphii = MorphiiData(id: morphii.id, name: morphii.name, scaleType: morphii.scaleType, metadata: metaData, displayName: displayName)
        self.currentMorphiiView = singleMorphiiView.morphiiView
        self.morphiiViews.append(singleMorphiiView.morphiiView)
        if configuration.morphiiConfig.showName {
            singleMorphiiView.nameLabel.text = morphii.name
        }else {
            singleMorphiiView.nameLabel.text = ""
        }
      let sideLength = frame.size.height - 85.0
      print("SIDE_LENGTH:",sideLength)
      let size = CGSize(width: sideLength, height: sideLength)
        singleMorphiiView.morphiiView.setUpMorphii(target: configuration.target, viewId: viewId, currentMorphii, size: size, intensity: configuration.options.initialIntensity)
        sendSelectionChangeEvent()
      singleMorphiiView.morphiiView.intensity = configuration.options.initialIntensity

    }
    
    func sendSelectionChangeEvent () {
        if let morphiiView = currentMorphiiView {
            
            let event = MorphiiChangeEvent(morphii: morphiiView.morphiiData,
                                           intensity: morphiiView.adjustedIntensity)
            let usage = UsageService(viewId: viewId, target: configurtation.target)
            usage.sendChangeEvent(data: event)
            delegate?.selectionDidChange(type: "selection_changed", selectionRequiredValid: true, view: BasicView.BasicViewObject(id: viewId, morphii: BasicView.MorphiiObject(selected: true, weight: configurtation.morphiiConfig.weightById(id: morphiiView.morphiiData.id))))
        }
    }
    
   public func submit (coord:MorphiiSDK.Coordinates, completion:@escaping ([ReactionResultRecord]) -> Void) {
    if let morphiiView = currentMorphiiView {
        let reaction = ReactionService()
        
        let morphii = ReactionMorphiiRequestRecord(morphii: morphiiView.morphiiData, intensity: morphiiView.adjustedIntensity)
        let reactionRecord = ReactionRequestRecord(morphii: morphii, target: configurtation.target, project: configurtation.project, options: configurtation.options, viewId: viewId, user: configurtation.user, commentText: commentTextView?.text)
        reaction.sendReaction(record: reactionRecord) {
            (results) in
            completion(results)
        }
      var commentText:String?
      if let text = commentTextView?.text.replacingOccurrences(of: " ", with: "") {
         if text != placeholder.replacingOccurrences(of: " ", with: "") {
            commentText = text
         }
      }
      let shareEvent = MorphiiShareSelectEvent(morphii: morphiiView.morphiiData, intensity: morphiiView.adjustedIntensity, text: commentText)
      UsageService().sendShareSelectEvent(data: shareEvent)
    }
   }
   
    func addMorphiiView (morphii:Morphii, width:CGFloat, height:CGFloat, x:CGFloat) {
      let morphiiViewSideLength = frame.size.height
      let morphiiView = MorphiiView(frame: CGRect(x: (frame.size.width / 2) - (morphiiViewSideLength / 2), y: 0, width: morphiiViewSideLength, height: morphiiViewSideLength))
      morphiiView.backgroundColor = UIColor.clear
      backgroundColor = UIColor.clear
      addSubview(morphiiView)

      let touchView = MorphiiTouchView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
      touchView.backgroundColor = UIColor.clear
      addSubview(touchView)
//      morphiiView.setUpMorphii(morphii, msize: CGSize(width: morphiiViewSideLength, height: morphiiViewSideLength), touchView: touchView)
   }
   
   required public init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }

    
    public func submit (coord:MorphiiSDK.Coordinates?, completion:@escaping (_ results:[ReactionResultRecord])->Void) {
      
      if let record = getReactionRequestRecord() {
         ReactionService().sendReaction(record: record) { (results) in
            completion(results)
         }
      }

    }
   
   func getReactionRequestRecord () -> ReactionRequestRecord? {
      if currentMorphiiView != nil {
         let request = ReactionRequestRecord(morphii: ReactionMorphiiRequestRecord(morphii: currentMorphiiView!.morphiiData, intensity: currentMorphiiView!.adjustedIntensity), target: configurtation.target, project: configurtation.project, options: configurtation.options, viewId: self.viewId, user: configurtation.user, commentText: commentTextView?.text)
         return request
      }
      return nil
   }
    
    public func reset (selection:Bool, comment:Bool) {
      if selection, let mutliView = morphiiLayout as? MultiMorphiiView{
         //unselect
        mutliView.shrinkUntappedMorphiis(tapView: nil)
        currentMorphiiView = nil
      }
      for morphiiView in morphiiViews {
         morphiiView.intensity = configurtation.options.initialIntensity
         morphiiView.setNeedsDisplay()
      }
      if comment {
         commentTextView?.text = self.placeholder
         commentTextView?.textColor = UIColor.lightGray
      }
    }
    
    public func png (size:CGSize) -> UIImage? {
        if AccountService.sharedInstance.isAuthenticated && isSubmitted {
            return currentMorphiiView?.getMorphiiImage()
        }else {
            return nil
        }

    }
    
    public class BasicViewObject {
        public var id:String!
      public var morphii:MorphiiObject?
      public var comment:CommentObject?
        init(id:String, morphii:MorphiiObject) {
            self.id = id
            self.morphii = morphii
        }
        
        
    }
    
    public class MorphiiObject {
        public var selected = false
        public var weight:Int = 0
        
        init(selected:Bool, weight:Int) {
            self.selected = selected
            self.weight =  weight
        }
    }
   
   public class CommentObject {
      public var required = false
      public var length:Int = 0
      public var value:String?
      
      init(required:Bool, length:Int, value:String?) {
         self.required = required
         self.length = length
         self.value = value
      }
   }
   
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        bindFrameToSuperviewBounds()
    }

}

extension UIView {
    
    func bindFrameToSuperviewBounds() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        let x = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1, constant: 0)
        let y = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1, constant: 0)
        let w = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: superview, attribute: .width, multiplier: 1, constant: 0)
        let h = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: superview, attribute: .height, multiplier: 1, constant: 0)
        
        superview.addConstraints([x,y,w,h])
    }
    
}
