//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import UIKit
import Foundation

class MorphiiView: UIView {
   
   var morphiiData: MorphiiData!
   var morphiiSize: CGSize!
   var renderEngine: RenderEngine!
   var changeEventData: ChangeEventData!
   private var usage: UsageService!

   var adjustedIntensity: Double {
      // Need to invert the value for scale type 1 so that Neutral is 0 and Max is 100.
      return self.morphiiData.scaleType == 1 ? (100 - self.intensity) : self.intensity
   }
   
   override init(frame: CGRect){
      super.init(frame: frame)
   }
   
   required init?(coder aDecoder: NSCoder){
      super.init(coder: aDecoder)
   }
   
   func setUpMorphii(target:Target, viewId:String, _ morphiiData: MorphiiData, size: CGSize, intensity: Double){
      self.morphiiData = morphiiData
      self.morphiiSize = size
      self.bounds.size = size
      self.setNeedsDisplay()
      self.renderEngine = RenderEngine(morphiiData: morphiiData, intensity: intensity)
      setUpMorphiiGestures()
      isUserInteractionEnabled = true
      usage = UsageService(viewId: viewId, target: target)
      self.changeEventData = ChangeEventData(intensity: intensity, previousIntensity: intensity)

   }
   
   func setUpMorphiiGestures(){
      print("setUpMorphiiGestures")
      let panNizer = UIPanGestureRecognizer()
      panNizer.addTarget(self, action: #selector(MorphiiView.showGestureForPanRecognizer(_:)))
      self.addGestureRecognizer(panNizer)
   }
   
   // This function handles setting the intensity as the user pans up and down on the morphii.
   func showGestureForPanRecognizer(_ recognizer: UIPanGestureRecognizer) {
      if recognizer.state == UIGestureRecognizerState.changed ||
         recognizer.state == UIGestureRecognizerState.ended
      {
         let translation:CGPoint = recognizer.translation(in: self)
         print("TRANSLATION:",translation)
         let scaleType = self.morphiiData.scaleType
         if (scaleType == 1 || scaleType == 2) {
            // NEUTRAL -> MIN / NEGATIVE EMOTIONS / SCALE TYPE 1
            // MAX -> NEUTRAL / POSITIVE EMOTIONS / SCALE TYPE 2
            self.intensity -= (Double(translation.y)) / 4.0
         }
         else if (scaleType == 3) {
            // MAX -> MIN / DOUBLE ANCHOR SCALE / SCALE TYPE 3
            self.intensity += (Double(translation.y)) / 4.0
         }
         else if (scaleType == 4) {
            // scale type 4 coming soon.
         }
         else {
            print("invalid scale type: \(scaleType)")
         }
         
         recognizer.setTranslation(CGPoint.zero, in: self)
         
         // Cap the intensity value between 0 - 100
         self.intensity = self.intensity > 100 ? 100 : self.intensity
         self.intensity = self.intensity < 0 ? 0 : self.intensity
         
         // Track the panning so that we can capture usage data.
         trackPanning(state: recognizer.state)
      }
   }
   
   // This function is used to track the panning gestures on the morphii and then
   // send usage data when needed.
   private func trackPanning(state: UIGestureRecognizerState) {
      // Need to invert the value for scale type 1 so that Neutral is 0 and Max is 100.
      let intensity = self.adjustedIntensity
      if state == UIGestureRecognizerState.ended {
         // Trigger Usage API.
         print("Trigger Usage - instensity change")
         let event = MorphiiIntensityChangeEvent(morphii: morphiiData, intensity: intensity, begin: self.changeEventData.previousIntensity, end: intensity)
         usage.sendChangeIntensityEvent(data: event)
         
         self.changeEventData.updatePanning(directionChange: false, value: intensity, direction: 0)
         self.changeEventData.previousIntensity = intensity
      }
      else if intensity != self.changeEventData.panning.value {
         // If the panning direction has changed direction, trigger the usage API.
         let direction = intensity > self.changeEventData.panning.value ? 1 : -1
         if self.changeEventData.panning.direction == 0 {
            self.changeEventData.panning.direction = direction
         }
         if self.changeEventData.panning.direction != direction {
            self.changeEventData.panning.direction = direction;
            self.changeEventData.panning.directionChange = true;
         }
         
         if self.changeEventData.panning.directionChange == true {
            self.changeEventData.panning.directionChange = false
            
            // Trigger Usage API.
            print("Trigger Usage - instensity change")
            let event = MorphiiIntensityChangeEvent(morphii: self.morphiiData, intensity: intensity, begin: self.changeEventData.previousIntensity, end: intensity)
            usage.sendChangeIntensityEvent(data: event)
            
            self.changeEventData.previousIntensity = intensity
         }
         self.changeEventData.panning.value = intensity
      }
   }
   
   var intensity: Double = 50.0 {
      didSet(newValue) {
         if self.renderEngine != nil {
            self.renderEngine.intensity = newValue
            self.setNeedsDisplay()
         }
      }
   }
   
   override func draw(_ rect: CGRect)
   {
      // determine how much to scale morphii:
      // morphii svg's are 478x478 but are drawn in 300x300 so here's the const:
      let scale = CGPoint(x: (self.morphiiSize.width / 478), y: (self.morphiiSize.height / 478))
      
      let context:CGContext = UIGraphicsGetCurrentContext()!
      context.scaleBy(x: scale.x, y: scale.y)
      
      self.backgroundColor = UIColor.clear
//      self.bounds.size = CGSize(width: 100, height: 100)
      self.setNeedsDisplay()
      
      // Render the morphii data.
      _ = self.renderEngine.render(context: context)
   }
   
   func getMorphiiImage () -> UIImage {
      
      backgroundColor = UIColor.clear
      let screenSize: CGRect = UIScreen.main.bounds
      var clipMorphySize = CGSize(width: screenSize.width, height: screenSize.height)
      UIGraphicsBeginImageContextWithOptions(clipMorphySize, false, 0.0)
      var context:CGContext = UIGraphicsGetCurrentContext()!
      self.layer.render(in: context)
      var snapShotImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
      

      
      //the cropping function
      func croppedImage(bounds:CGRect) -> UIImage{
         let imageRef:CGImage = snapShotImage.cgImage!.cropping(to: bounds)!
         let croppedImage:UIImage = UIImage(cgImage: imageRef)
         
         return croppedImage
      }
      
      func imageResize(image: UIImage, size: CGSize) -> UIImage {
         //let scale = UIScreen.main.scale
         let scale = UIScreen.main.scale
         UIGraphicsBeginImageContextWithOptions(size, false, scale)
         let context = UIGraphicsGetCurrentContext()
         //context!.interpolationQuality = CGInterpolationQuality.high
         context!.interpolationQuality = .high
         let originPoint = CGPoint(x: 0, y: 0)
         //image.draw(in: CGRect(origin: originPoint, size: size))
         image.draw(in: CGRect(origin: originPoint, size: size))
         let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         
         return scaledImage!
      }
      
      //bounding rectangle for cropping morphy's image
      //must be in pixels because of the conversion from UIImage to CGImageRef
      //width is longer so morphii will fit in text bubble
      
      //check for iphone 6+ and set scale to 3 or 2 for all other devices
      let scaleVal = (UIScreen.main.nativeScale >= 2.6 ? 3.0 : 2.0)
      let cropRect: CGRect = CGRect(x: 0, y: 0, width: self.frame.width * CGFloat(scaleVal), height: self.frame.width * CGFloat(scaleVal))
      
      //crop the snapshot with the bounding rectangle
      let morphyPic = croppedImage(bounds: cropRect)
      
      //image insets
      let morphiiInsets = UIEdgeInsetsMake(10, 20, 10, 20)
      let morphiiPic = morphyPic.newImageWithInsets(insets: morphiiInsets)
      
      let morphiiImageSize = CGSize(width: morphiiPic!.size.width/10, height: morphiiPic!.size.height/10)
      let morphPic = imageResize(image: morphiiPic!, size: morphiiImageSize)
      
      //end the graphics context
      UIGraphicsEndImageContext()
      self.layer.contents = nil
        self.setNeedsDisplay()

      return morphPic
      
   }
}

extension UIImage {
   func newImageWithInsets(insets: UIEdgeInsets) -> UIImage? {
      UIGraphicsBeginImageContextWithOptions(
         CGSize(width: self.size.width + insets.left + insets.right,
                height: self.size.height + insets.top + insets.bottom), false, self.scale)
      let _ = UIGraphicsGetCurrentContext()
      let origin = CGPoint(x: insets.left, y: insets.top)
      //self.draw(at: origin)
      self.draw(at: origin)
      let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      return imageWithInsets
   }
   
}
