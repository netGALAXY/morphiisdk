//
//  MorphiiAPI.swift
//  morphii
//
//  Created by Corley Higgins on 3/10/16.
//  Copyright © 2016 Viizbi. All rights reserved.
//
//


import Foundation
import Alamofire


class MorphiiAPI {
    
    var allMorphiis = [ Morphii ]()
    
    //emojione morphiis
    //let morphiisUrl = "https://api-dev.morphii.com/morphii/v1/morphiis?&accountId=2016-07-20541211&includePngData=true&includeMetadata=true&groupName=EmojiOne"
    
    //Core morphiis
    //let morphiisUrl = "https://api-dev.morphii.com/morphii/v1/morphiis?&accountId=2016-07-20541211&includePngData=true&includeMetadata=true&groupName=Core%20Morphiis"
    
    static var username:String!
    static var password:String!
    static var accountId:String!
    static var clientKey:String!
    
    static var apiKey = ""
    static var jwt = ""
    
    //more morphiis
    
    
    func getUrl (api:API) -> String {
        if api == .AUTHENTICATE {
            return Constants.API.Authenticate
        }else if api == .MORPHIIS {
            return Constants.API.Morphiis + "?&accountId="+MorphiiAPI.accountId+"&ids="
        }else if api == .REACTIONS {
            return Constants.API.Reactions
        }else {
            return Constants.API.Usage
        }
    }
    
    func getHeaders (api:API) -> [String:String]{
        if api == .MORPHIIS || api == .REACTIONS {
            print("JWT:",MorphiiAPI.jwt, "APIKEY:",MorphiiAPI.apiKey)
            let headers = [
                "x-api-key": MorphiiAPI.apiKey,
                "Content-Type":" application/json",
                "Authorization":MorphiiAPI.jwt
            ]
            return headers
        }else if api == .AUTHENTICATE {
            return ["Content-Type":" application/json"]
        }else {
            return [
                "x-api-key": MorphiiAPI.apiKey, "Content-Type":" application/json"
            ]
        }
    }
    
    
    func getMorphiiIDs () -> [String] {
        return ["6202185110603694080", "6202185104312238080", "6202185104333209600", "6202185110939238400"]
    }
    
    func getAuthenticateBodyData () -> Parameters {
        let data = ["account_id":MorphiiAPI.accountId!, "username":MorphiiAPI.username!,"password":MorphiiAPI.password!]
        return data
    }
    
    func getReactionBodyData () -> Parameters {
        return ["view_id":"sdfsfd",
                "account_id":MorphiiAPI.accountId!,
                "reaction_record":
                    ["application":
                        ["id":"sdk",
                         "instance":"iOS",
                         "version":"1.0",
                         "properties":["prop1":"value1", "prop2":"value2"]],
                     "client":
                        ["session": "432ca8cd-1e65-4a11-9dec-94235a7bf653",
                         "lat":0,
                         "lng":0,
                         "device":
                            ["id": "66650371-39B5-46C5-A5BC-93C39BBABA05",
                             "manufacturer":"Apple",
                             "model":"iPhone 6",
                             "firmware":"10.1.1",
                             "make":"iPhone"]
                            ],
                     "timestamp":
                        ["utc": "2016-12-16T17:56:10.552Z",
                         "offset":"-0500"],
                     "morphii":
                        ["id":"6202184382145363968",
                         "name":"OMG!",
                         "intensity":0.2,
                         "display_name":"Excited",
                         "weight":"0"],
                     "target":
                        ["id":"question-1",
                         "type":"question",
                         "metadata": ["prop1":"value1", "prop2":"value2"]],
                     "account":
                        ["id":"85390159"],
                     "state":
                        ["stage":"test"],
                     "comment":
                        ["text":"This is my comment",
                         "Locale":"en-US"],
                     "project":
                        ["id":"project-id",
                         "description":"The project description"]
                    ]
                ]
    }
    
    func fetchMorphiis(ids:[String], _ completion: @escaping (_ morphiisArray: [ Morphii ]) -> Void ) -> Void {
        let morphiisUrl = getUrl(api: .MORPHIIS)+ids.joined(separator: ",")
        print("URL:",morphiisUrl)
        Alamofire.request(morphiisUrl, headers: getHeaders(api: .MORPHIIS)).responseJSON { response in
//            
//            print("\(response.request)")  // original URL request
//            print("\(response.response)") // HTTP URL response
//            print("\(response.data)")     // server data
            print("\(response.result)")   // result of response serialization
            if let JSON = response.data {
                if let morphiis = self.processAllMorphiisData(data: JSON, error: response.result) {
                    completion(morphiis)
                    
                }else {
                    completion([])
                }
                //let morphii = self.buildMorphii(data: JSON, error: error)
            }
        }
        
        
    }
    
    
    func fetchAllMorphiis(_ completion: @escaping (_ morphiisArray: [ Morphii ]) -> Void ) -> Void {
        let morphiisUrl = getUrl(api: .MORPHIIS)+getMorphiiIDs().joined(separator: ",")
        print("URL:",morphiisUrl)
        Alamofire.request(morphiisUrl, headers: getHeaders(api: .MORPHIIS)).responseJSON { response in
//            
//            print("\(response.request)")  // original URL request
//            print("\(response.response)") // HTTP URL response
//            print("\(response.data)")     // server data
//            print("\(response.result)")   // result of response serialization
            if let JSON = response.data {
                if let morphiis = self.processAllMorphiisData(data: JSON, error: response.result) {
                    completion(morphiis)
                    
                }else {
                    completion([])
                }
                //let morphii = self.buildMorphii(data: JSON, error: error)
            }
        }
        
        
    }
    
    func submitReaction () {
        let parameters = getAuthenticateBodyData()
        guard let url = URL(string: getUrl(api: .REACTIONS)), let data = getJSONData(parameters: parameters) else {
            print("authenticate1")
            //completion(false, nil, nil, nil, nil)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        let headers = getHeaders(api: .REACTIONS)
        for key in headers.keys {
            request.setValue(headers[key], forHTTPHeaderField: key)
        }

        request.httpBody = data
        
        
        
        Alamofire.request(request)
            .responseJSON { response in
                
                if let code = response.response?.statusCode, code != 200 {
                    if let JSON = response.data, let _ = self.parseJSON(data: JSON) as? [String:String] {
                        //completion(false, dictionary, nil, nil, nil)
                    }else {
                        //completion(false, nil, nil, nil, nil)
                    }
                }
                if let JSON = response.data, let dictionary = self.parseJSON(data: JSON) as? [String:Any] {
                    print("REACTIONS1:", dictionary)
                    //completion(true, nil, jwt, apiKey, sessionId)
                }else {
                    print("REACTIONS2")
                    //completion(false, nil, nil, nil, nil)
                }
                
        }
    }
    
    //process data to get morphii names and ids for collection
    func processAllMorphiisData(data: Data?, error: Any?) -> [ Morphii ]? {
        
        //parse data and find morphiis in a collection
        guard let morphiiData = data else {
            print("There has been an error processing request: \(error)")
            return nil
        }
        
        print("---------------------- "+String(data: morphiiData, encoding: String.Encoding.utf8)!)

        guard let jsonDict = parseJSON(data: morphiiData), let morphiiRecords: [ NSDictionary ] = jsonDict.object(forKey: "records") as? Array else {return nil}
        var allMorphiis = [ Morphii ]()
        
        for (i, record) in morphiiRecords.enumerated(){
            let data = record.value(forKey: "data") as? NSDictionary
            let metaData = data?.value(forKey: "metaData") as? NSDictionary
            let scaleType = record.value(forKey: "scaleType") as? Int
            let recId = record.value(forKey: "id") as? String
            let recName = record.value(forKey: "name") as? String
            let recStaticUrl = record.value(forKey: "staticUrl") as? String
            let recDataUrl = record.value(forKey: "dataUrl") as? String
            let recSequence = morphiiRecords[i].value(forKey: "sequence") as? Int
            let pngDataString = data?.value(forKey: "png") as? String
            
            let morphii = Morphii(id: recId, name: recName, scaleType: scaleType, staticUrl: recStaticUrl, dataUrl: recDataUrl, sequence: recSequence, pngString: pngDataString, metaData: metaData)
            
            allMorphiis.append(morphii)
        }
        
        return allMorphiis
        
    }
   
   func parseJSON (data:Data) -> NSDictionary? {
      var jsonDict:NSDictionary? = nil
      
      do {
         
         jsonDict = try (JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)as? NSDictionary)
         return jsonDict
      }
      catch{
         print("Handle \(error) here")
         return nil
      }
   }
   
    func authenticate (username:String, password:String, accountId:String, clientKey:String, completion:@escaping (_ success:Bool, _ error:[String:String]?, _ jwt:String?, _ apiKey:String?, _ sessionId:String?) -> Void) {
        let parameters = getReactionBodyData()
        guard let url = URL(string: getUrl(api: .AUTHENTICATE)), let data = getJSONData(parameters: parameters) else {
         print("authenticate1")
            completion(false, nil, nil, nil, nil)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data

        
        
        Alamofire.request(request)
            .responseJSON { response in
                
                if let code = response.response?.statusCode, code != 200 {
                    if let JSON = response.data, let dictionary = self.parseJSON(data: JSON) as? [String:String] {
                        completion(false, dictionary, nil, nil, nil)
                    }else {
                        completion(false, nil, nil, nil, nil)
                    }
                }
               if let JSON = response.data, let dictionary = self.parseJSON(data: JSON) as? [String:Any], let jwt = dictionary["jwt"] as? String, let apiKey = dictionary["api_key"] as? String, let sessionId = dictionary["session_id"] as? String {
                print("AUTHENTICATE1:", dictionary)
                MorphiiAPI.apiKey = apiKey
                MorphiiAPI.jwt = jwt
                completion(true, nil, jwt, apiKey, sessionId)
               }else {
                  completion(false, nil, nil, nil, nil)
               }
                
        }

    }
    
    private func getJSONData (parameters:Parameters) -> Data? {
        do {
        let data = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                return json.data(using: String.Encoding.utf8.rawValue);

            }else {
                return nil
            }
            
        }catch {
            return nil
        }
    }
    
    enum API:String {
        case AUTHENTICATE = "authenticate"
        case MORPHIIS = "morphiis"
        case REACTIONS = "reactions"
        case USAGE = "usage"
    }
    
}








