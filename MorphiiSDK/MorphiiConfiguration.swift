//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class define a morphii size.
@objc public class MorphiiSize: NSObject
{
    var initialSize: Int;  // For future use.
    var selectedSize: Int; // Currently valid only for single morphii config.

    public init(initialSize: Int = 0, selectedSize: Int = 0) {
        self.initialSize = initialSize
        self.selectedSize = selectedSize
    }
}

// This class defines the Morphiis configuration information.
@objc public class MorphiiConfiguration: NSObject {

    // This private class defines the Morphii config details.
    private class MorphiiDetail {
        
        let id: String
        let name: String?
        let weight: Int
        let size: MorphiiSize?  // Currently valid only for single morphii config.
        
        init(id: String, name: String?, size: MorphiiSize?, weight: Int = 0) {
            self.id = id
            self.name = name
            self.weight = weight
            self.size = size
        }
        
        // This function returns the json object for the configuration.
        func json() -> [String: Any] {
            var json: [String: Any] = [
                "id": self.id,
                "weight": self.weight
            ]
            
            if self.size != nil {
                json["size"] = [
                    "initial_size": (self.size?.initialSize)!,
                    "selected_size": (self.size?.selectedSize)!
                ]
            }
            
            if self.name != nil {
                json["name"] = self.name!
            }
            
            return json
        }
    }
    
    public let showName: Bool
    private var morphiiDetails: [MorphiiDetail] = [MorphiiDetail]()
    
    public init(showName: Bool = true) {
        self.showName = showName
    }
    
    // This function adds a MorphiiId object to the list.
    public func add(id: String, name: String?, size: MorphiiSize?, weight: Int = 0) {
        let detail = MorphiiDetail(id: id, name: name, size: size, weight: weight)
        morphiiDetails.append(detail)
    }

    // This function returns the assigned weight for the given morphii id.
    func weightById(id: String) -> Int {
        if let detail = morphiiDetails.filter({$0.id == id}).first {
            return detail.weight
        }
        return 0
    }

    // This function returns the display name if available for the given morphii id.
    func displayNameById(id: String) -> String? {
        if let detail = morphiiDetails.filter({$0.id == id}).first {
            return detail.name
        }
        return nil
    }
    
    // This function returns the display name if available for the given morphii id.
    func sizeById(id: String) -> MorphiiSize? {
        if let detail = morphiiDetails.filter({$0.id == id}).first {
            return detail.size
        }
        return nil
    }
    
    // This function returns an array of morphii id string in the order they were inserted via the add function.
    func ids() -> [String] {
        let ids = self.morphiiDetails.map({
            (value: MorphiiDetail) -> String in
            return value.id
        })
        return ids
    }
    
    // This function returns the json object for the configuration.
    func json() -> [String: Any] {
        let ids = self.morphiiDetails.map({
            (value: MorphiiDetail) -> [String: Any] in
            return value.json()
        })
        
        return [
            "show_name": self.showName,
            "ids": ids
        ]
    }
}
