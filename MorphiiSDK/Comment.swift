//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the Comment configuration information.
@objc public class Comment: NSObject {

    let show: Bool
    let required: Bool
    let maxLength: Int
    let label: String
    let hintText: String
    
    public init(show: Bool = true, required: Bool = false, maxLength: Int = 255, label: String = "Please leave a comment", hintText: String = "Please leave a comment") {
        self.show = show
        self.required = required
        self.maxLength = maxLength
        self.label = label
        self.hintText = hintText
    }

    // This function returns the json object for the configuration.
    func json() -> [String: Any] {
        return [
            "show": self.show,
            "required": self.required,
            "max_length": self.maxLength,
            "label": self.label,
            "hint_text": self.hintText
        ]
    }
}

