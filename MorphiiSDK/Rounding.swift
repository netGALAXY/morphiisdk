//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This helper class provides a function to round a double to decimal places value.
class Rounding {
    
    // Function to round a double to decimal places value.
    static func roundTo(_ number: Double, places:Int = 4) -> Double {
        return number.roundTo(places: places)
    }
}

// Help to round the double to decimal places value
extension Double {
    // Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
