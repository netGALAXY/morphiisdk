//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// A class to define a reaction result record.
public class ReactionResultRecord {
    
    public let isSubmitted: Bool
    public let reactionId: String?
    public let viewId: String
    public let targetId: String
    public let comment: ReactionCommentRecord?
    public let morphii: ReactionMorphiiResultRecord?
    public var error: ReactionError?
    
    init(record: NSDictionary) {
        
        self.isSubmitted = record.value(forKey: "submitted") as! Bool
        self.targetId = record.value(forKey: "target_id") as! String
        self.viewId = record.value(forKey: "view_id") as! String
        
        if self.isSubmitted == false {
            // Sample of what the record looks like:
            //  {
            //    "records": [
            //      {
            //        "submitted": false,
            //        "view_id": "view-id",
            //        "target_id": "test-target-id",
            //        "error": {
            //          "code": "Conflict",
            //          "message": "\"morphii.name\" is required"
            //        }
            //      }
            //    ],
            //    "count": 1
            //  }
            
            // Get the error data.
            if let errorData = record["error"] as? NSDictionary {
                let code: String = errorData.value(forKey: "text") as! String
                let message: String = errorData.value(forKey: "locale") as! String
                self.error = ReactionError(code: code, message: message)
            }
            else {
                self.error = nil
            }
            self.comment = nil
            self.reactionId = nil
            self.morphii = nil
        }
        else {
            self.error = nil
            self.reactionId = record.value(forKey: "reaction_id") as? String
            
            // Check if the detail data is available.
            // Detail record may not be in results if the clients subscription
            // has expired or is over its limit.
         print("RECORD_RECORD:",record)

            if let detailRecord = record["reaction_record"] as? NSDictionary {
                // Sample of what the record looks like:
                //  {
                //    "records": [
                //      {
                //        "submitted": true,
                //        "view_id": "view-id",
                //        "target_id": "test-target-id",
                //        "reaction_id": "6220641847904043008",
                //        "reaction_record": {
                //          "id": "6220641847904043008",
                //          /* More reaction properties */
                //        }
                //      }
                //    ],
                //    "count": 1
                //  }

                // Get the morphii data.
                if let morphiiDetail = detailRecord["morphii"] as? NSDictionary {
                    let id: String = morphiiDetail.value(forKey: "id") as! String
                    let name: String = morphiiDetail.value(forKey: "name") as! String
                    let displayName: String = morphiiDetail.value(forKey: "display_name") as! String
                    let intensity: Double = morphiiDetail.value(forKey: "intensity") as! Double
                    let weight: Int = morphiiDetail.value(forKey: "weight") as! Int
                    self.morphii = ReactionMorphiiResultRecord(id: id, name: name, displayName: displayName, intensity: intensity, weight: weight)
                }
                else {
                    self.morphii = nil
                }

                // Get the comment data.
                if let comment = detailRecord["comment"] as? NSDictionary {
                    let text: String = comment.value(forKey: "text") as! String
                    let locale: String = comment.value(forKey: "locale") as! String
                    self.comment = ReactionCommentRecord(text: text, locale: locale)
                }
                else {
                    self.comment = nil
                }
            }
            else {
                self.comment = nil
                self.morphii = nil
            }
        }
    }
}
