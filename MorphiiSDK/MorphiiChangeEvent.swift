//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines the morphii change event
class MorphiiChangeEvent : MorphiiEvent {
    
    // NOTE: Intensity should be a value from 0 - 100. The event will divide this by 100 internally.
    override init(morphii: MorphiiData, intensity: Double) {
        super.init(morphii: morphii, intensity: intensity)
    }
}
