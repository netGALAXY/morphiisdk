//
//  MultiMorphiiCommentView.swift
//  MorphiiSDK
//
//  Created by Charlie  on 1/26/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit

class MultiMorphiiCommentView: MultiMorphiiView {
    @IBOutlet weak var commentTitleLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    var placeHolder = "Placeholder"
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
        commentTextView.layer.borderColor = UIColor.darkGray.cgColor
        commentTextView.layer.borderWidth = 1.0
        commentTextView.delegate = self
        commentTextView.text = placeHolder
        commentTextView.textColor = UIColor.lightGray
    }

}

extension MultiMorphiiCommentView:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var valid = true
        if configuration.comment.required {
            if textView.text.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: " ", with: "") == "" {
                valid = false
            }
        }
        var morphiiId = ""
        if let ident = selectedMorphiiId {
            morphiiId = ident
        }
        basicView.delegate?.commentChange(type: "comment_changed", commentRequiredValid: valid, view: BasicView.BasicViewObject(id: basicView.viewId, morphii: BasicView.MorphiiObject(selected: basicView.currentMorphiiView != nil, weight: configuration.morphiiConfig.weightById(id: morphiiId))))
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
}
