import Foundation
import Alamofire

public class Morphii {
    
    var id: String = ""
    var name: String = ""
    var scaleType: Int = 0
    var staticUrl: String = ""
    var dataUrl: String = ""
    //let changedDate: String
    //let category: String
    //let keywords: [ String ]
    var sequence: Int = 0
    var pngString: String?
    var metaData: NSDictionary?
    let emoodl = 50.0
    
    init(id: String?, name: String?, scaleType: Int?, staticUrl: String?, dataUrl: String?, /*changedDate: String, category: String, keywords: [ String ],*/ sequence: Int?, pngString: String?, metaData: NSDictionary?) {
        if let value = id {
            self.id = value
        }
        if let value = name {
            self.name = value
        }
        if let value = scaleType {
            self.scaleType = value
        }
        if let value = staticUrl {
            self.staticUrl = value
        }
        if let value = dataUrl {
            self.dataUrl = value
        }
        //self.changedDate = changedDate
        //self.category = category
        //self.keywords = keywords
        if let value = sequence {
            self.sequence = value
        }
        if let value = pngString {
            self.pngString = value
        }
        if let value = metaData {
            self.metaData = value
        }
    }
    
    func getCorrectedEmoodl (emoodl:Double) -> Double {
        var newEmoodl = emoodl
        if scaleType == 1 {
            newEmoodl = 100.0 - newEmoodl
        }
        return newEmoodl
    }
    
    
    
}

class MorphiiIndex {
    
    let id: String
    let name: String
    var staticUrl: String
    var dataUrl:String
    let scaleType: Int
    var morphiiImage: UIImage?
    var morphiiData: Data?
    
    
    init(id: String, name: String, staticUrl:String, dataUrl:String, scaleType:Int) {
        self.id = id
        self.name = name
        self.staticUrl = staticUrl
        self.dataUrl = dataUrl
        self.scaleType = scaleType
    }
    
    func setStaticImage(_ morphiiImage:UIImage){
        self.morphiiImage = morphiiImage
    }
    
    func setMorphiiData(_ morphiiData:Data){
        self.morphiiData = morphiiData
    }
    
}
