//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class holds the change event data.
// This class helps provide the values needed for the usage data.
class ChangeEventData {
    var intensity: Double
    var previousIntensity: Double
    var panning: Panning

    init(intensity: Double, previousIntensity: Double) {
        self.intensity = intensity
        self.previousIntensity = previousIntensity
        self.panning = Panning(directionChange: false, value: self.intensity, direction: 0)
    }
    
    func updatePanning(directionChange: Bool, value: Double, direction: Int) {
        self.panning = Panning(directionChange: directionChange, value: value, direction: direction)
    }
    
    struct Panning {
        var directionChange: Bool
        var value: Double
        var direction: Int
    }
}
