//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation
import Alamofire


// https://grokswift.com/rest-with-alamofire-swiftyjson/

// This class POST the usage data to the Morphii service.
class UsageService {
    
    let viewId: String?
    let target: Target?
    
    init(viewId: String? = nil, target: Target? = nil) {
        self.viewId = viewId
        self.target = target
    }
    
    // Function to send the 'sdk-load' event.
    func sendLoadEvent() {
        let events: [Parameters] = [
            [
                "type": "sdk-load",
                "timestamp": Timestamp.timestamp,
            ]
        ]

        var json: Parameters = self.common()
        json["events"] = events
        
        post(jsonObject: json)
    }

    // Function to send the 'view-add' event.
    func sendAddEvent(data: ViewAddEvent) {
        let events: [Parameters] = [
            [
                "type": "view-add",
                "timestamp": Timestamp.timestamp,
                "properties": [
                    "options": data.options
                ]
            ]
        ]
        
        var json: Parameters = self.common(target: self.target)
        json["events"] = events
        
        post(jsonObject: json)
    }

    // Function to send the 'morphii-change' event.
    func sendChangeEvent(data: MorphiiChangeEvent) {
        let events: [Parameters] = [
            [
                "type": "morphii-change",
                "timestamp": Timestamp.timestamp,
                "properties": [
                    "morphii": self.morphii(data: data)
                ]
            ]
        ]
        
        var json: Parameters = self.common(target: self.target)
        json["events"] = events

        post(jsonObject: json)
    }

    // Function to send the 'morphii-intensity-change' event.
    func sendChangeIntensityEvent(data: MorphiiIntensityChangeEvent) {
        let events: [Parameters] = [
            [
                "type": "morphii-intensity-change",
                "timestamp": Timestamp.timestamp,
                "properties": [
                    "morphii": self.morphii(data: data),
                    "intensity": [
                        "begin": data.begin,
                        "end": data.end
                    ]
                ]
            ]
        ]

        var json: Parameters = self.common(target: self.target)
        json["events"] = events

        post(jsonObject: json)
    }

    // Function to send the 'morphii-share-select' event.
    func sendShareSelectEvent(data: MorphiiShareSelectEvent) {
        var properties: Parameters = [
            "morphii": self.morphii(data: data)
        ]
        
        // Add text if available.
        if data.text != nil {
            properties["share"] = [
                "text": data.text
            ]
        }
        
        let events: [Parameters] = [
            [
                "type": "morphii-share-select",
                "timestamp": Timestamp.timestamp,
                "properties": properties
            ]
        ]

        var json: Parameters = self.common(target: self.target)
        json["events"] = events

        post(jsonObject: json)
    }

    // Helper property to return the common JSON object.
    private func common(target: Target? = nil) -> [String: Any] {
        var json: Parameters = [
            "application": ApplicationInformation.application(viewId: self.viewId),
            "account": AccountService.sharedInstance.account,
            "client": ClientInformation.client
        ]
        
        if target != nil {
           json["target"] = target?.json()
        }
        
        return json
    }
    
    // Helper function to return morphii JSON object.
    private func morphii(data: MorphiiEvent) -> [String: Any] {
        return [
            "id": data.id,
            "name": data.name,
            "intensity": data.intensity,
            "display_name": data.displayName,
            "weight": data.weight
        ]
    }
    
    // Function to post to the usage API.
    private func post(jsonObject: Parameters) {
        let backgroundQueue = DispatchQueue(label: "com.morphii.queue.usage", qos: .background, target: nil)
        backgroundQueue.async {
            let url: String = Constants.API.Usage
            let headers = [
                "x-api-key": AccountService.sharedInstance.apiKey,
                "Content-Type": "application/json"
            ]
        
            Alamofire.request(url, method: .post, parameters: jsonObject, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { response in
                    guard response.result.error == nil else {
                        // got an error in getting the data, need to handle it
                        print("error calling POST")
                        print(response.result.error!)
                        return
                    }
                
                    let statusCode = (response.response?.statusCode)! as Int
                    if statusCode != 201 {
                        // make sure we got some JSON since that's what we expect
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get object as JSON from API")
                            print("Error: \(response.result.error)")
                            return
                        }
                        // Error making API call.
                        // TODO: Need to do something if there is an error.
                        print("Error getting data")
                        print("\(json)")
                    }
                    else {
                        // Do nothing.
                        print("POST call was successful - no content")
                    }
            }
        }
    }
}
