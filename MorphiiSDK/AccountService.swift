//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation
import Alamofire

class AccountService {

    private var userName: String
    private var password: String

    var isAuthenticated: Bool
    var id: String
    var jwt: String
    var apiKey: String
    var sessionId: String
    var error: ReactionError
    


    
    // Property to get account JSON object.
    var account: [String: Any] {
        return [
            "id": self.id
        ]
    }

    static let sharedInstance : AccountService = {
        let instance = AccountService()
        return instance
    }()

    // Authenticate and validate the user. This static function returns a boolean.
    // If function returns false then check the AccountService.sharedInstance.error
    // If function returns true then the values (jwt, apiKey, and sessionId) will be populated.
    func authenticate(username: String, password: String, accountId: String, completion:@escaping (_ results:MorphiiSDK.AuthenticationResults) -> Void) {

        // Call the authentication API. This should be a sync call.
        self.userName = username
        self.password = password
        self.id = accountId

        let json: [String: Any] = [
            "username": username,
            "password": password,
            "account_id": accountId
        ]
            
        self.post(jsonObject: json, completion: completion)
    }
    
    private init() {
        self.isAuthenticated = false
        self.id = ""
        self.jwt = ""
        self.apiKey = ""
        self.sessionId = ""
        self.userName = ""
        self.password = ""
        self.error = ReactionError(code: "", message: "")
    }
    
    // Function to post to authenticate API. This is a sync call.
    private func post(jsonObject: Parameters, completion:@escaping (_ results:MorphiiSDK.AuthenticationResults) -> Void) {
        let url: String = Constants.API.Authenticate
        let headers = [
            "Content-Type": "application/json"
        ]
            
        Alamofire.request(url, method: .post, parameters: jsonObject, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                guard response.result.error == nil else {
                    // error in getting the data, need to handle it
                    print("Error calling API: \(response.result.error!)")
                    self.error = ReactionError(code: "Unauthorized", message: "Error authenticating user")
                    self.isAuthenticated = false
                    completion(MorphiiSDK.AuthenticationResults(isAuthenticated: false, jwt: nil, apiKey: nil, sessionId: nil, error: self.error))
                    return
                }
                    
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("No data in results: \(response.result.error)")
                    self.error = ReactionError(code: "Unauthorized", message: "No data in results")
                    self.isAuthenticated = false
                    completion(MorphiiSDK.AuthenticationResults(isAuthenticated: false, jwt: nil, apiKey: nil, sessionId: nil, error: self.error))
                    return
                }

                let statusCode = (response.response?.statusCode)! as Int
                if statusCode == 200 {
                    // Get the data returned.
                    self.jwt = json["jwt"] as! String
                    self.apiKey = json["api_key"] as! String
                    self.sessionId = json["session_id"] as! String
                    self.isAuthenticated = true
                    MorphiiAPI.apiKey = self.apiKey
                    MorphiiAPI.jwt = self.jwt
                    completion(MorphiiSDK.AuthenticationResults(isAuthenticated: true, jwt: self.jwt, apiKey: self.apiKey, sessionId: self.sessionId, error: nil))
                }
                else {
                    // There was an error authenticating the user.
                    // ["errorMessage": {"code":"Unauthorized","message":"Wrong email or password."}]
                    let jsonString = json["errorMessage"] as! String
                    let data = jsonString.data(using: .utf8)!
                    if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any] {
                        let code = parsedData["code"] as! String
                        let message = parsedData["message"] as! String
                        self.error = ReactionError(code: code, message: message)
                    }
                    
                    self.isAuthenticated = false
                    completion(MorphiiSDK.AuthenticationResults(isAuthenticated: false, jwt: nil, apiKey: nil, sessionId: nil, error: self.error))
                }
        }
    }
}
