//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the Selection configuration information.
@objc public class Selection: NSObject {

    let required: Bool
    
    public init(required: Bool = false) {
        self.required = required
    }


    // This function returns the json object for the configuration.
    func json() -> [String: Any] {
        return [
            "required": self.required
        ]
    }
}
