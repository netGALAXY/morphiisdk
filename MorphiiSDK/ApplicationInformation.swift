//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This helper class provides application information.
class ApplicationInformation {
    
    // Function returns the application JSON object.
    // Note the viewId should be passed in if known.
    static func application(viewId: String?) -> [String: Any] {
        var json: [String: Any] = [
            "id": "sdk",
            "instance": "iOS",
            "version": "1.0"
        ]
        
        if viewId != nil {
            json["properties"] = [
                "view_id": viewId!
            ]
        }
        
        return json
    }
}
