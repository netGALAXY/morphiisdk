//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the configuration information for the basic view.
@objc public class BasicViewConfiguration: NSObject {

    let morphiiConfig: MorphiiConfiguration
    let target: Target
    let project: Project
    let comment: Comment
    let selection: Selection
    let options: Options
    let user: User?
   
    
   public init(morphiiConfig: MorphiiConfiguration, target: Target, project: Project, comment: Comment = Comment(), selection: Selection = Selection(), options: Options = Options(), user: User? = nil) {
        self.morphiiConfig = morphiiConfig
        self.target = target
        self.project = project
        self.comment = comment
        self.selection = selection
        self.options = options
        self.user = user
    }
        
    // This function returns the json object for the configuration.
    func json() -> [String: Any] {
        var json: [String: Any] = [
            "morphiis": self.morphiiConfig.json(),
            "comment": self.comment.json(),
            "selection": self.selection.json(),
            "options": self.options.json(),
            "target": self.target.json(),
            "project": self.project.json()
        ]

        if self.user != nil {
            json["user"] = self.user?.json()
        }

        return json
    }
}
