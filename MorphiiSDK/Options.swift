//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the Options configuration information.
@objc public class Options: NSObject {

    let stage: String
    let initialIntensity: Double
    
    public init(stage: String = "live", initialIntensity: Double = 0.5) {
        self.stage = stage
        self.initialIntensity = initialIntensity * 100.0
    }

    // This function returns the json object for the configuration.
    func json() -> [String: Any] {        
        return [
            "initial_intensity": self.initialIntensity / 100.0,
            "stage": self.stage
        ]
    }
}
