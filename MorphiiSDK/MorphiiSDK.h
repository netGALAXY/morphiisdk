//
//  MorphiiSDK.h
//  MorphiiSDK
//
//  Created by Charlie  on 1/6/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MorphiiSDK.
FOUNDATION_EXPORT double MorphiiSDKVersionNumber;

//! Project version string for MorphiiSDK.
FOUNDATION_EXPORT const unsigned char MorphiiSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MorphiiSDK/PublicHeader.h>
