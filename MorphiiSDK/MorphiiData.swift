//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines a morphii record from the morphii REST API results.
public class MorphiiData {
    
    let id: String
    let name: String
    let scaleType: Int
    let metadata: NSDictionary
    let displayName: String
    let weight: Int
    
   public init(id: String, name: String, scaleType: Int, metadata: NSDictionary, displayName: String, weight: Int = 0) {
        self.id = id
        self.name = name
        self.scaleType = scaleType
        self.metadata =  metadata
        self.displayName =  displayName
        self.weight =  weight
    }
}
