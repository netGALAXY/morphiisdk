//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// A class to define a reaction request record.
class ReactionRequestRecord {
    
    let morphii: ReactionMorphiiRequestRecord
    let target: Target
    let project: Project
    let options: Options
    let user: User?
    let commentText: String?
    let viewId: String
    
    init(morphii: ReactionMorphiiRequestRecord, target: Target, project: Project, options: Options, viewId: String, user: User? = nil, commentText: String? = nil) {

        self.morphii = morphii
        self.target = target
        self.project = project
        self.options = options
        self.user = user
        self.viewId = viewId
        self.commentText = commentText
    }
    
    // This function returns the json object for a reaction.
    func json() -> [String: Any] {
        var json: [String: Any] = [
            "application": ApplicationInformation.application(viewId: self.viewId),
            "client": ClientInformation.client,
            "timestamp": Timestamp.timestamp,
            "morphii": self.morphii.json(),
            "target": self.target.json(),
            "account": [
                "id": AccountService.sharedInstance.id
            ],
            "state": [
                "stage": self.options.stage
            ],
            "project": [
                "id": self.project.id,
                "description": self.project.description
            ]
        ]
        
        // Add comment section if needed.
        if self.commentText != nil {
            var locale = NSLocale.preferredLanguages[0]
            if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                locale += "-" + countryCode
            }
            
            json["comment"] = [
                "text": self.commentText,
                "locale": locale
            ]
        }
        
        // Add user section if needed.
        if self.user != nil {
            json["user"] = self.user?.json()
        }

        return json
    }
}
