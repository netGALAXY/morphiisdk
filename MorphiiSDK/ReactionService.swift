//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation
import Alamofire


// This class POST the usage data to the Morphii service.
class ReactionService {

    // This function sends all the reactions that are in currently in the records list.
    func sendReaction(record: ReactionRequestRecord, completion:@escaping ([ReactionResultRecord]) -> Void) {
        
        var records: [ReactionRequestRecord] = [ReactionRequestRecord]()
        records.append(record)
        
        self.sendReactions(records: records) {
            (results) in
            print("results: \(results)")
            
            completion(results)
        }
    }

    // This function sends all the reactions that are in currently in the records list.
    func sendReactions(records: [ReactionRequestRecord], completion:@escaping ([ReactionResultRecord]) -> Void) {
        
        let reactions = records.map({
            (value: ReactionRequestRecord) -> [String: Any] in
            return value.json()
        })
        
        let body: Parameters = [
            "account_id": AccountService.sharedInstance.id,
            "records": reactions
        ]

        self.post(jsonObject: body) {
            (results) in
            completion(results)
        }
    }
    
    // Function to post to the reactions API.
    private func post(jsonObject: Parameters, completion:@escaping ([ReactionResultRecord]) -> Void) {
        let url: String = Constants.API.Reactions
        let headers = [
            "x-api-key": AccountService.sharedInstance.apiKey,
            "Content-Type": "application/json",
            "Authorization": AccountService.sharedInstance.jwt
        ]
      print("JSON_OBJECT_FROM_REACTION:",jsonObject)
        
        Alamofire.request(url, method: .post, parameters: jsonObject, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling POST")
                    print(response.result.error!)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                                
                var records = [ReactionResultRecord]()
                let statusCode = (response.response?.statusCode)! as Int
                if statusCode == 201 {
                  print("RESULT_RECORD:",json)
                    let reactionRecords: [NSDictionary] = json["records"] as! Array
                    for (_, record) in reactionRecords.enumerated() {
                        records.append(ReactionResultRecord(record: record))
                    }
                }

                completion(records)
        }
    }
}
