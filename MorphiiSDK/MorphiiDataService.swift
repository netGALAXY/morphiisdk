//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation
import Alamofire

// This class GETs the morphii data from the Morphii service.
class MorphiiDataService {
    
    var hashId: String
    var count: Int
    var morphiiDataRecords: [MorphiiData]
    
    init() {
        self.hashId = ""
        self.count = 0
        self.morphiiDataRecords = [MorphiiData]()
    }

    // This function gets the morphii data from the given morphii ids.
    func getMorphiis(morphiiConfig: MorphiiConfiguration, completion: @escaping (_ morphiisArray: [MorphiiData]) -> Void ) -> Void {
        // Create URL.
        var url = Constants.API.Morphiis
        url += "?accountId="
        url += AccountService.sharedInstance.id
        url += "&ids="
        url += morphiiConfig.ids().joined(separator: ",")
        
        let headers = [
            "x-api-key": AccountService.sharedInstance.apiKey,
            "Content-Type": "application/json",
            "Authorization": AccountService.sharedInstance.jwt
        ]
        
        Alamofire.request(url, method: .get, headers: headers)
            .responseJSON { response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET")
                    print(response.result.error!)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    print("didn't get object as JSON from API")
                    print("Error: \(response.result.error)")
                    return
                }
                
                let statusCode = (response.response?.statusCode)! as Int
                if statusCode == 200 {
                    self.hashId = json["hash_id"] as! String
                    self.count = json["count"] as! Int
                    
                    let morphiiRecords: [ NSDictionary ] = json["records"] as! Array
                    for (_, record) in morphiiRecords.enumerated() {
                        let data = record.value(forKey: "data") as! NSDictionary
                        let metadata = data.value(forKey: "metaData") as! NSDictionary
                        let scaleType = record.value(forKey: "scaleType") as! Int
                        let id = record.value(forKey: "id") as! String
                        let name = record.value(forKey: "name") as! String
                    
                        // Get the display name and weigth, if provided.
                        let weight: Int = morphiiConfig.weightById(id: id)
                        var displayName: String? = morphiiConfig.displayNameById(id: id)
                        displayName = displayName == nil ? name : displayName
                        let record: MorphiiData = MorphiiData(id: id, name: name, scaleType: scaleType, metadata: metadata, displayName: displayName!, weight: weight)
                        self.morphiiDataRecords.append(record)
                    }

                    completion(self.morphiiDataRecords)
                }
                else {
                    // Error making API call.
                    // TODO: Need to do something if there is an error.
                    print("Error getting data")
                    print("\(json)")
                }
        }
    }
}
