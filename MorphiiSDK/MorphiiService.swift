//
//  MorphiiService.swift
//  MorphiiSDK
//
//  Created by Charles von Lehe on 1/11/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation

@objc public class MorphiiService: NSObject {
    var user:User!
    public var project:Project!
   var basicViews:[BasicView] = []
   private static var shared:MorphiiService!
    public static var production = true

   @objc public class func sharedInstance () -> MorphiiService {
    Constants.API.setUrls() 
      if shared == nil {
         shared = MorphiiService()
      }
      return shared
   }
    
    private override init () {
        
    }
    
    public func authenticate (username:String, password:String, accountId:String, completion:@escaping (_ results:MorphiiSDK.AuthenticationResults)->Void) {
        MorphiiAPI.username = username
        MorphiiAPI.password = password
        MorphiiAPI.accountId = accountId
        
        AccountService.sharedInstance.authenticate(username: username, password: password, accountId: accountId) { (authenticationResults) in
         UsageService().sendLoadEvent()

            completion(authenticationResults)
        }
    }
    
    public func add(containerView:UIView, config:BasicViewConfiguration, delegate:BasicViewDelegate?) -> BasicView {
      let frame = CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height)
      let basicView = BasicView(frame: frame, configuration: config, delegate: delegate)
      basicViews.append(basicView)
      let viewAddEvent = ViewAddEvent(options: config.options.json())
      UsageService().sendAddEvent(data: viewAddEvent)
      return basicView

    }
    
   public func submit (coord:MorphiiSDK.Coordinates?, completion:@escaping ([ReactionResultRecord]) -> Void) {
      var records:[ReactionRequestRecord] = []
    
      for basicView in basicViews {
         print("BASIC_VIEW:",basicView)
         if !basicView.isSubmitted, let record =  basicView.getReactionRequestRecord(), basicView.currentMorphiiView != nil {
            records.append(record)
            basicView.isSubmitted = true
            if let morphiiView = basicView.currentMorphiiView {
               var commentText:String?
               if let text = basicView.commentTextView?.text.replacingOccurrences(of: " ", with: "") {
                  if text != basicView.placeholder.replacingOccurrences(of: " ", with: "") {
                     commentText = text
                  }
            }
                let shareEvent = MorphiiShareSelectEvent(morphii: morphiiView.morphiiData, intensity: morphiiView.adjustedIntensity, text: commentText)
                UsageService().sendShareSelectEvent(data: shareEvent)
            }
         }
      }
      
      ReactionService().sendReactions(records: records, completion: completion)

    }
    
    public func reset (selection:Bool, comment:Bool) {
      for basicView in basicViews {
         basicView.reset(selection: selection, comment: comment)
      }
    }
    

}
