//
//  Project.swift
//  MorphiiSDK
//
//  Created by Charlie  on 1/17/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation

// This class defines the Project configuration information.
@objc public class Project: NSObject {
   
   let id: String
   let projectDescription: String
   
  public init(id: String, description: String) {
      self.id = id
      self.projectDescription = description
   }
   
   // This function returns the json object for the configuration.
   func json() -> [String: Any] {
      return [
         "id": self.id,
         "description": self.projectDescription
      ]
   }
}
