//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This helper class provides timestamp information such as the timestamp JSON object and time zone offset.
class Timestamp {
    
    // Property to get current time zone.
    static var timeZoneOffset: String {
        let offset = NSTimeZone.local.secondsFromGMT() / 60
        return (offset < 0 ? "-" : "+") + String(Swift.abs(offset) / 60).leftPadding(toLength: 2, withPad: "0") + String(Swift.abs(offset) % 60).leftPadding(toLength: 2, withPad: "0")
    }
    
    // Property to get timestamp JSON object.
    static var timestamp: [String: Any] {
        return [
            "utc": Date().iso8601,
            "offset": timeZoneOffset
        ]
    }
}


// Helper for getting ISO 8601 formatted date.
extension Date {
    struct Formatter {
        static let iso8601: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
            return formatter
        }()
    }
    
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}


// Helper for left padding a string.
extension String {
    func leftPadding(toLength: Int, withPad character: Character) -> String {
        let newLength = self.characters.count
        if newLength < toLength {
            return String(repeatElement(character, count: toLength - newLength)) + self
        } else {
            return self.substring(from: index(self.startIndex, offsetBy: newLength - toLength))
        }
    }
}



