//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines the morphii base event
class MorphiiEvent {
    let id: String
    let name: String
    let displayName: String
    let intensity: Double
    let weight: Int
    
    // NOTE: Intensity should be a value from 0 - 100. The event will divide this by 100 internally.
    init(morphii: MorphiiData, intensity: Double) {
        self.id = morphii.id
        self.name = morphii.name
        self.displayName = morphii.displayName
        self.weight = morphii.weight
        self.intensity = Rounding.roundTo(intensity / 100.0)
    }
}
