//
//  SingleMorphiiCommentView.swift
//  MorphiiSDK
//
//  Created by Charles von Lehe on 1/19/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit

class SingleMorphiiCommentView: UIView {
    @IBOutlet weak var morphiiView: MorphiiView!

    @IBOutlet weak var commentTitleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    var basicView:BasicView!
    var placeHolder = "Placeholder"
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
        commentTextView.layer.borderColor = UIColor.darkGray.cgColor
        commentTextView.layer.borderWidth = 1.0
        commentTextView.delegate = self
        commentTextView.text = placeHolder
        commentTextView.textColor = UIColor.lightGray
    }
 

}

extension SingleMorphiiCommentView:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var valid = true
        if basicView.configurtation.comment.required {
            if textView.text.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: " ", with: "") == "" {
                valid = false
            }
        }

        basicView.delegate?.commentChange(type: "comment_changed", commentRequiredValid: valid, view: BasicView.BasicViewObject(id: basicView.viewId, morphii: BasicView.MorphiiObject(selected: basicView.currentMorphiiView != nil, weight: basicView.configurtation.morphiiConfig.weightById(id: morphiiView.morphiiData.id))))
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
}
