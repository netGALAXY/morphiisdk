//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines the Target configuration information.
@objc public class Target: NSObject {

    let id: String
    let type: String
    var metadata: NSDictionary?
    
    public init(id: String, type: TargetType, metadata: NSDictionary? = nil) {
        self.id = id
        self.type = type.rawValue
        self.metadata = metadata
    }

    // This function returns the json object for the configuration.
    func json() -> [String: Any] {
        var json: [String: Any] = [
            "id": self.id,
            "type": self.type
        ]
         
        if self.metadata != nil {
            var data = [String: Any]()
            for (value, key) in self.metadata! {
                data[key as! String] = value
            }
            json["metadata"] = data
        }

        return json
    }
   
   public enum TargetType:String {case Question = "question", Video = "video", Tweet = "tweet", Article = "article",Link = "link",Other = "other"}
}
