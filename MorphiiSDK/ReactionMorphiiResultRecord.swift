//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the morphii reaction result data
public class ReactionMorphiiResultRecord {
    
    public let id: String
    public let name: String
    public let displayName: String
    public let intensity: Double
    public let weight: Int
    
    public init(id: String, name: String, displayName: String, intensity: Double, weight: Int) {
        self.id = id
        self.name = name
        self.displayName = displayName
        self.intensity = intensity
        self.weight = weight
    }
}
