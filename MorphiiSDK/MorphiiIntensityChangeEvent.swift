//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines the morphii intensity change event
class MorphiiIntensityChangeEvent : MorphiiEvent {
    let begin: Double
    let end: Double
    
    // NOTE: Intensity should be a value from 0 - 100. The event will divide this by 100 internally.
    init(morphii: MorphiiData, intensity: Double, begin: Double, end: Double) {
        self.begin = Rounding.roundTo(begin / 100.0)
        self.end = Rounding.roundTo(end / 100.0)

        super.init(morphii: morphii, intensity: intensity)
    }
}
