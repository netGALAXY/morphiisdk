//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines the morphii share select event
class ViewAddEvent {

    let options: [String: Any]
    
    init(options: [String: Any]) {
        self.options = options
    }
}
