//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

struct Constants {

    struct API {
        static var Authenticate: String = "https://api.morphii.com/sdk/v1/authenticate"
        static var Morphiis: String = "https://api.morphii.com/sdk/v1/morphiis"
        static var Reactions: String = "https://api.morphii.com/sdk/v1/reactions"
        static var Usage: String = "https://api.morphii.com/analytics/v1/usage"
        static var Echo = "https://api-dev.morphii.com/echo/v1/echo"
        
        static func setUrls () {
            if !MorphiiService.production {
                Authenticate = "https://api-dev.morphii.com/sdk/v1/authenticate"
                Morphiis = "https://api-dev.morphii.com/sdk/v1/morphiis"
                Reactions = "https://api-dev.morphii.com/sdk/v1/reactions"
                Usage = "https://api-dev.morphii.com/analytics/v1/usage"
            }
        }
    }
}

