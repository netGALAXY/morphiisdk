//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines the morphii share select event
class MorphiiShareSelectEvent : MorphiiEvent {
    
    let text: String?
    
    // NOTE: Intensity should be a value from 0 - 100. The event will divide this by 100 internally.
    init(morphii: MorphiiData, intensity: Double, text: String?) {
        self.text = text

        super.init(morphii: morphii, intensity: intensity)
    }
}
