//
//  MultiMorphiiView.swift
//  MorphiiSDK
//
//  Created by Charles von Lehe on 1/26/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit

class MultiMorphiiView: UIView {

    @IBOutlet weak var containerView: UIView!
   var viewId:String!
   var configuration:BasicViewConfiguration!
    var basicView:BasicView!
    var morphiiContainerWidth = CGFloat(0)
    var morphiiContainerHeight = CGFloat(0)
    var selectedMorphiiId:String?
    var unselectedConstant = CGFloat(0)
    var selectedConstant = CGFloat(0)

   var morphiiContainerViews:[MorphiiContainerView] = []
    var previousView:UIView?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
//        containerView.layer.cornerRadius = 24
//      containerView.translatesAutoresizingMaskIntoConstraints = false
      translatesAutoresizingMaskIntoConstraints = false
//        containerView.clipsToBounds = true
      
    }
   
    func setupMorphiis (morphiiArray:[Morphii], configuration:BasicViewConfiguration, basicView:BasicView) {
        addCircleViews()

      self.configuration = configuration
        self.basicView = basicView
      layoutIfNeeded()
      var morphiis:[Morphii] = []
      if morphiiArray.count < 7 {
         morphiis.append(contentsOf: morphiiArray)
      }else {
         for i in 0 ..< 7 {
            morphiis.append(morphiiArray[i])
         }
      }

      morphiiContainerWidth = (containerView.frame.size.width / CGFloat(morphiis.count))
            print("CONTAINER_WIDTH:",containerView.frame.size.width,"CONTAINER_HEIGHT:",containerView.frame.size.height,"MORPHII_CONTAINER_WIDTH:",morphiiContainerWidth)
      morphiiContainerHeight = containerView.frame.size.height


      
      var x = CGFloat(0)
      for morphii in morphiis {

         let morphiiContainerView = MorphiiContainerView(frame: CGRect(x: x, y: containerView.frame.origin.y, width: morphiiContainerWidth, height: morphiiContainerHeight))
        x += morphiiContainerWidth

        setupMorphiiContainerView(morphiiContainerView: morphiiContainerView, morphii: morphii)
        setupMorphiiView(morphiiContainerView: morphiiContainerView, morphii: morphii)


      }
   }
    
    func setupMorphiiView (morphiiContainerView:MorphiiContainerView, morphii:Morphii) {
        
        var morphiiSideLength = containerView.frame.size.height
      if morphiiSideLength > morphiiContainerWidth {
         morphiiSideLength = morphiiContainerWidth - 15.0
      }
//        if let size = configuration.morphiiConfig.sizeById(id: morphii.id) {
//            morphiiSideLength = CGFloat(size.initialSize)
//        }
//        
//        if morphiiSideLength > containerView.frame.size.height {
//            morphiiSideLength = containerView.frame.size.height
//        }else if morphiiSideLength > morphiiContainerWidth {
//            morphiiSideLength = morphiiContainerWidth
//        }
        unselectedConstant = morphiiSideLength
        selectedConstant = morphiiSideLength + 30.0

        
        let morphiiView = MorphiiView(frame: CGRect(x: (morphiiContainerWidth/2.0) - (morphiiSideLength / 2.0), y: (morphiiContainerHeight/2.0) - (morphiiSideLength / 2.0) - 10.0, width: morphiiSideLength, height: morphiiSideLength))
        
        morphiiView.translatesAutoresizingMaskIntoConstraints = false
        morphiiView.backgroundColor = UIColor.clear
        morphiiContainerView.addSubview(morphiiView)
        
        morphiiContainerView.morphiiView = morphiiView
        morphiiView.isUserInteractionEnabled = false
        basicView.morphiiViews.append(morphiiView)
        addConstraintsForMorphiiView(morphiiView: morphiiView, morphiiContainerView: morphiiContainerView, morphiiSideLength:morphiiSideLength)
        
        setupMorphii(morphiiView: morphiiView, morphii: morphii, morphiiSideLength: morphiiSideLength)
        addCoverView(morphiiContainerView: morphiiContainerView)
        if configuration.morphiiConfig.showName {
            addNameLabelForMorphiiView(morphiiView: morphiiView, morphii: morphii, morphiiContainerView: morphiiContainerView)
        }
    }
    
    func setupMorphiiContainerView (morphiiContainerView:MorphiiContainerView, morphii:Morphii) {
        morphiiContainerViews.append(morphiiContainerView)
        morphiiContainerView.backgroundColor = UIColor.clear
        morphiiContainerView.morphii = morphii
        
        addSubview(morphiiContainerView)
        morphiiContainerView.translatesAutoresizingMaskIntoConstraints = false
        addConstraintsForContainerView(morphiiContainerView: morphiiContainerView, morphiiContainerWidth: morphiiContainerWidth, previousView: previousView)
        
        previousView = morphiiContainerView
        morphiiContainerView.isUserInteractionEnabled = true
        morphiiContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MultiMorphiiView.morphiiContainerViewTapped(_:))))

    }
    
    func addNameLabelForMorphiiView (morphiiView:MorphiiView, morphii:Morphii, morphiiContainerView:MorphiiContainerView) {
        var title = morphii.name
        if let display = configuration.morphiiConfig.displayNameById(id: morphii.id) {
            title = display
        }
        layoutIfNeeded()
        let labelHeight = CGFloat(17)
        let labelY = morphiiView.frame.origin.y + morphiiView.frame.size.height
        var constant = CGFloat(0)
        let containerBottom = containerView.frame.origin.y + containerView.frame.size.height
        if labelY < containerBottom {
            constant = containerView.frame.origin.y + containerView.frame.size.height - labelY - labelHeight
            print("CONTAINER_BOTTUM:",containerBottom, "LABELY:",labelY,"CONSTANT:",constant)
        }
        let label = UIButton(frame: CGRect(x: morphiiView.frame.origin.x, y: labelY, width: morphiiView.frame.size.width, height: labelHeight))
        label.translatesAutoresizingMaskIntoConstraints = false
        label.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        label.setTitleColor(UIColor.black, for: .normal)
        label.setTitle(title, for: .normal)
        label.titleLabel?.lineBreakMode = .byTruncatingTail
        addSubview(label)
        
        let x = NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: morphiiView, attribute: .centerX, multiplier: 1, constant: 0)
        let y = NSLayoutConstraint(item: morphiiView, attribute: .bottom, relatedBy: .equal, toItem: label, attribute: .top, multiplier: 1, constant: -constant)
        let w = NSLayoutConstraint(item: label, attribute: .width, relatedBy: .equal, toItem: morphiiContainerView, attribute: .width, multiplier: 1, constant: 0)
        let h = NSLayoutConstraint(item: label, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: labelHeight)
        
        addConstraints([x, y, w, h])
        
        layoutIfNeeded()
    }
    func addCircleViews () {
        let sideLength = containerView.frame.size.height
        let circleView1 = UIView(frame: CGRect(x: containerView.frame.origin.x - (sideLength / 2), y: containerView.frame.origin.y, width: sideLength, height: sideLength))
        let circleView2 = UIView(frame: CGRect(x: containerView.frame.origin.x + containerView.frame.size.width - (sideLength / 2), y: containerView.frame.origin.y, width: sideLength, height: sideLength))
        
        circleView1.backgroundColor = containerView.backgroundColor
        circleView2.backgroundColor = containerView.backgroundColor
        
        circleView1.translatesAutoresizingMaskIntoConstraints = false
        circleView2.translatesAutoresizingMaskIntoConstraints = false
        circleView1.layer.cornerRadius = sideLength / 2
        circleView1.clipsToBounds = true
        circleView2.layer.cornerRadius = sideLength / 2
        circleView2.clipsToBounds = true
        
        addSubview(circleView1)
        addSubview(circleView2)
        
        let x1 = NSLayoutConstraint(item: circleView1, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1, constant: -(sideLength / 2))
        let y1 = NSLayoutConstraint(item: circleView1, attribute: .centerY, relatedBy: .equal, toItem: containerView, attribute: .centerY, multiplier: 1, constant: 0)
        let w1 = NSLayoutConstraint(item: circleView1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sideLength)
        let h1 = NSLayoutConstraint(item: circleView1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sideLength)
        
        let x2 = NSLayoutConstraint(item: circleView2, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1, constant: (sideLength / 2))
        let y2 = NSLayoutConstraint(item: circleView2, attribute: .centerY, relatedBy: .equal, toItem: containerView, attribute: .centerY, multiplier: 1, constant: 0)
        let w2 = NSLayoutConstraint(item: circleView2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sideLength)
        let h2 = NSLayoutConstraint(item: circleView2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sideLength)
        
        addConstraints([x1,y1,w1,h1,x2,y2,w2,h2])
    }
    
    func addCoverView (morphiiContainerView:MorphiiContainerView) {
        morphiiContainerView.coverView = UIView(frame: CGRect(x: 0, y: 0, width: morphiiContainerView.frame.size.width, height: morphiiContainerView.frame.size.height))
        morphiiContainerView.coverView.backgroundColor = UIColor.clear
        morphiiContainerView.addSubview(morphiiContainerView.coverView)
    
        let x = NSLayoutConstraint(item: morphiiContainerView.coverView, attribute: .centerX, relatedBy: .equal, toItem: morphiiContainerView, attribute: .centerX, multiplier: 1, constant: 0)
        let y = NSLayoutConstraint(item: morphiiContainerView.coverView, attribute: .centerY, relatedBy: .equal, toItem: morphiiContainerView, attribute: .centerY, multiplier: 1, constant: 0)
        let w = NSLayoutConstraint(item: morphiiContainerView.coverView, attribute: .width, relatedBy: .equal, toItem: morphiiContainerView, attribute: .width, multiplier: 1, constant: 0)
        let h = NSLayoutConstraint(item: morphiiContainerView.coverView, attribute: .height, relatedBy: .equal, toItem: morphiiContainerView, attribute: .height, multiplier: 1, constant: 0)

        addConstraints([x,y,w,h])
    }
   
   func setupMorphii (morphiiView:MorphiiView, morphii:Morphii, morphiiSideLength:CGFloat) {
      var metaData = NSDictionary()
      if let data = morphii.metaData {
         metaData = data
      }
      var displayName = ""
      if let display = configuration.morphiiConfig.displayNameById(id: morphii.id) {
         displayName = display
      }
      let morphiiData = MorphiiData(id: morphii.id, name: morphii.name, scaleType: morphii.scaleType, metadata: metaData, displayName: displayName)
      let size = CGSize(width: morphiiSideLength, height: morphiiSideLength)
      
      morphiiView.setUpMorphii(target: configuration.target, viewId: viewId, morphiiData, size: size, intensity: configuration.options.initialIntensity)
   }

   
   func addConstraintsForContainerView (morphiiContainerView:MorphiiContainerView, morphiiContainerWidth:CGFloat, previousView:UIView?) {

      var x = NSLayoutConstraint(item: morphiiContainerView, attribute: .leadingMargin, relatedBy: .equal, toItem: containerView, attribute: .leadingMargin, multiplier: 1, constant: 0)
      if let previous = previousView {
         x = NSLayoutConstraint(item: previous, attribute: .right, relatedBy: .equal, toItem: morphiiContainerView, attribute: .left, multiplier: 1, constant: 0)
      }
      let y = NSLayoutConstraint(item: morphiiContainerView, attribute: .centerY, relatedBy: .equal, toItem: containerView, attribute: .centerY, multiplier: 1, constant: 0)
      morphiiContainerView.witdthConstraint = NSLayoutConstraint(item: morphiiContainerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: morphiiContainerWidth)
      let height = NSLayoutConstraint(item: morphiiContainerView, attribute: .height, relatedBy: .equal, toItem: containerView, attribute: .height, multiplier: 1, constant: 0)
      
      addConstraints([x,y,morphiiContainerView.witdthConstraint,height])
   }
   
   func addConstraintsForMorphiiView (morphiiView:MorphiiView, morphiiContainerView:MorphiiContainerView, morphiiSideLength:CGFloat) {
      let x = NSLayoutConstraint(item: morphiiView, attribute: .centerX, relatedBy: .equal, toItem: morphiiContainerView, attribute: .centerX, multiplier: 1, constant: 0)
      let y = NSLayoutConstraint(item: morphiiView, attribute: .centerY, relatedBy: .equal, toItem: morphiiContainerView, attribute: .centerY, multiplier: 1, constant: 0)
      morphiiContainerView.morphiiWidthConstraint = NSLayoutConstraint(item: morphiiView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: morphiiSideLength)
      morphiiContainerView.morphiiHeightConstraint = NSLayoutConstraint(item: morphiiView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: morphiiSideLength)
      
      addConstraints([x,y,morphiiContainerView.morphiiWidthConstraint,morphiiContainerView.morphiiHeightConstraint])
   }
   
    func morphiiContainerViewTapped (_ tap:UITapGestureRecognizer) {
        print("TAP")
        if let tapView = tap.view as? MorphiiContainerView {
            sendSelectionChangeEvent(tapView: tapView)

            if !tapView.enlarged {
                selectedMorphiiId = tapView.morphii.id
                growTappedMorphii(tapView: tapView)
                shrinkUntappedMorphiis(tapView: tapView)
                UIView.animate(withDuration: 0.3, animations: {
                    self.layoutIfNeeded()
                }, completion: { (success) in
                    for morphiiContainerView in self.morphiiContainerViews {
                        self.setupMorphii(morphiiView: morphiiContainerView.morphiiView, morphii: morphiiContainerView.morphii, morphiiSideLength: morphiiContainerView.morphiiWidthConstraint.constant)
                    }
                })
               
            }else {
                shrinkUntappedMorphiis(tapView: nil)
            }
        }
    }
    
    func sendSelectionChangeEvent (tapView:MorphiiContainerView) {
        let event = MorphiiChangeEvent(morphii: tapView.morphiiView.morphiiData, intensity: tapView.morphiiView.adjustedIntensity)
        let usage = UsageService(viewId: basicView.viewId, target: configuration.target)
        usage.sendChangeEvent(data: event)
        basicView.delegate?.selectionDidChange(type: "selection_changed", selectionRequiredValid: true, view: BasicView.BasicViewObject(id: basicView.viewId, morphii: BasicView.MorphiiObject(selected: true, weight: configuration.morphiiConfig.weightById(id: tapView.morphii.id))))
    }
    
    func growTappedMorphii (tapView:MorphiiContainerView) {
//        var selectedConstant = tapView.morphiiWidthConstraint.constant + 30.0
//        if let selected = configuration.morphiiConfig.sizeById(id: tapView.morphii.id)?.selectedSize {
//            selectedConstant = CGFloat(selected)
//        }
        
        tapView.morphiiWidthConstraint.constant = selectedConstant
        tapView.morphiiHeightConstraint.constant = selectedConstant
        basicView.currentMorphiiView = tapView.morphiiView
        tapView.enlarged = true
        tapView.coverView.isHidden = true
        tapView.morphiiView.isUserInteractionEnabled = true
    }
    
    func shrinkUntappedMorphiis (tapView:MorphiiContainerView?) {
        var tapViewId = "none"
        if let tView = tapView {
            tapViewId = tView.morphii.id
        }
        for morphiiContainerView in morphiiContainerViews {
            if morphiiContainerView.morphii.id != tapViewId && morphiiContainerView.enlarged{
//                var initalConstant = morphiiContainerView.morphiiWidthConstraint.constant - 30.0
//                if let inital = configuration.morphiiConfig.sizeById(id: morphiiContainerView.morphii.id)?.initialSize {
//                    initalConstant = CGFloat(inital)
//                }
                morphiiContainerView.morphiiWidthConstraint.constant = unselectedConstant
                morphiiContainerView.morphiiHeightConstraint.constant = unselectedConstant
                morphiiContainerView.enlarged = false
                morphiiContainerView.coverView.isHidden = false
                morphiiContainerView.morphiiView.isUserInteractionEnabled = false
            }
        }
    }
   
   class MorphiiContainerView:UIView {
      var witdthConstraint:NSLayoutConstraint!
      var morphiiWidthConstraint:NSLayoutConstraint!
      var morphiiHeightConstraint:NSLayoutConstraint!
      var morphiiView:MorphiiView!
      var morphii:Morphii!
      var enlarged = false
    var coverView:UIView!
    
   }

}
