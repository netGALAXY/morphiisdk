//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the configuration information for the basic view.
class TestBasicViewConfig {

    static func SampleConfiguration() -> BasicViewConfiguration {
        //
        // Test the BasicViewConfiguration
        //
        let comment: Comment = Comment()
        let options: Options = Options()
        let selection: Selection = Selection()
        let project: Project = Project(id: "sample-project-id", description: "Sample project description")

        // Target
        let metadata : NSDictionary = ["value1" : "key1", "values2" : "key2", 123 : "key3"]
        let target: Target = Target(id: "test-target-id", type: Target.TargetType.Video, metadata: metadata)
        
        // User
        let properties : NSDictionary = ["sample@test.com" : "email", "values2" : "key2", 987654 : "internal_id"]
        let user: User = User(id: "user-id", type: "external", properties: properties)
        
        // Morphii ids
        let morphiiConfig: MorphiiConfiguration = MorphiiConfiguration()
        morphiiConfig.add(id: "6202185110603694080", name: "Pensive-1", size: nil)
        morphiiConfig.add(id: "6202185104312238080", name: nil, size: nil)
        morphiiConfig.add(id: "6202185104333209600", name: "Heart Eyes-1", size: nil)
        morphiiConfig.add(id: "6202185110939238400", name: "Eye Roll-1", size: nil)
        
        // Create the basic view config
      return BasicViewConfiguration(morphiiConfig: morphiiConfig, target: target, project: project, comment: comment, selection: selection, options: options, user: user)
    }
}
