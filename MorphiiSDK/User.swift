//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation

// This class defines the Project configuration information.
@objc public class User: NSObject {
    
    let id: String
    let type: String
    let properties: NSDictionary?
    
    public init(id: String, type: String, properties: NSDictionary? = nil) {
        self.id = id
        self.type = type
        self.properties = properties
    }
    
    // This function returns the json object for the configuration.
    func json() -> [String: Any] {
        var json: [String: Any] = [
            "id": self.id,
            "type": self.type
        ]
        
        if self.properties != nil {
            var data = [String: Any]()
            for (value, key) in self.properties! {
                data[key as! String] = value
            }
            json["properties"] = data
        }
        
        return json
    }
}
