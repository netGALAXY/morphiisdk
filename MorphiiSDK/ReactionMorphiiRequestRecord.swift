//
// Copyright (C) Vizbii, Inc - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
//

import Foundation


// This class defines the morphii reaction request data
public class ReactionMorphiiRequestRecord {
    
    let id: String
    let name: String
    let displayName: String
    let intensity: Double
    let weight: Int
    
    // NOTE: Intensity should be a value from 0 - 100. The value will be divided by 100 internally.
    public init(morphii: MorphiiData, intensity: Double) {
        self.id = morphii.id
        self.name = morphii.name
        self.displayName = morphii.displayName
        self.weight = morphii.weight
        self.intensity = Rounding.roundTo(intensity / 100.0)
    }
    
    // This function returns the json object for a reaction.
    func json() -> [String: Any] {
        return [
            "id": self.id,
            "name": self.name,
            "display_name": self.displayName,
            "intensity": self.intensity,
            "weight": self.weight
        ]
    }
}
